package fenci;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;  
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;  
import java.util.HashMap;  
import java.util.Iterator;  
import java.util.StringTokenizer;  

import java.util.Date;
import java.text.SimpleDateFormat;

/*import java.io.IOException;  
import java.io.StringReader;  
import org.wltea.analyzer.core.IKSegmenter;  
import org.wltea.analyzer.core.Lexeme;*/
  
public class cipin {  
    public static void main(String[] args)  throws IOException {  
        //用HashMap存放<单词:词频>这样一个映射关系  
        HashMap<String, Integer> hashMap=new HashMap<String, Integer>();  
        //用正则表达式来过滤字符串中的所有标点符号  
        String regex = "[[A-Z][a-z][0-9]|_@&-.。,\"!--;:?\'\\]]";      
    	String path = "D:\\WEBTXT\\" + File.separator;// 要创建的文件的位置
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        try {  
            //读取要处理的文件  
            BufferedReader br=new BufferedReader(new FileReader("D://WEBTXT//webtxt.txt"));  
            /*StringReader sr=new StringReader("D://WEBTXT//webtxt.txt");  
            IKSegmenter ik=new IKSegmenter(sr, true);  
            Lexeme lex=null;  
            while((lex=ik.next())!=null){  
                System.out.print(lex.getLexemeText()+"|");  
            }  */
            String value;  
            while((value=br.readLine())!=null){  
                value=value.replaceAll(regex, " ");  
                //使用StringTokenizer来分词(StringTokenizer详见JDK文档)  
                StringTokenizer tokenizer = new StringTokenizer(value);  
                while(tokenizer.hasMoreTokens()){  
                    String word=tokenizer.nextToken();  
                    if(!hashMap.containsKey(word)){    
                        hashMap.put(word, new Integer(1));    
                    }else{    
                        int k=hashMap.get(word).intValue()+1;    
                        hashMap.put(word, new Integer(k));    
                    }    
                }  
            }  
           
            
            try {
                BufferedWriter out = new BufferedWriter(new FileWriter((path + "ouline.txt"), true));
                //遍历HashMap,输出结果  
                Iterator iterator=hashMap.keySet().iterator();    
                out.write(df.format(new Date()));// new Date()为获取当前系统时间
                out.write("\r\n");
                while(iterator.hasNext()){    
                    String word=(String) iterator.next();    
                    //System.out.println(word+":\t"+hashMap.get(word));  
                    if (hashMap.get(word)>10){ // 只统计热度大于10的词频
                    out.write(word+":\t"+hashMap.get(word));
                    out.write("\r\n");}
                }  
                out.write("\r\n");
                out.write("\r\n");
                out.write("\r\n");
                out.write("\r\n");
                out.close();
                System.out.println("Add Success");  
            } catch (IOException e) {
            }

            
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }  
}  

