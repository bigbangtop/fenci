package fenci;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URI;
import java.net.URLConnection;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

public class webtxt {

	public static void main(String[] args) throws Exception {
		URI uri = URI.create("https://www.hao123.com/");
		URLConnection connection = uri.toURL().openConnection();
		InputStream input = connection.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(input, "utf-8"));
		String line;
		// System.out.println(line);
		String path = "D:\\WEBTXT\\" + File.separator;// 要创建的文件的位置
		// 创建文件夹
		File newdirpath = new File(path);
		if (!newdirpath.exists()) {// 判断是否已经存在，不存在即创建
			newdirpath.mkdirs();
		} else {// 存在便删除，然后再创建
			newdirpath.delete();
			System.out.println("File Delete success.");
			newdirpath.mkdirs();
			System.out.println("File Create Success.");
		}
		// 创建txt文件并输出结果
		try {
			BufferedWriter file = new BufferedWriter(new FileWriter(path + "webtxt.txt"));
			file.write("null");
			file.close();
		} catch (IOException e) {
		}
		File file = new File(path + "webtxt.txt");
		//file.createNewFile();

		BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
		while ((line = br.readLine()) != null) {
			//bw.write(line);
			StringReader sr=new StringReader(line);  
	        IKSegmenter ik=new IKSegmenter(sr, true);  
	        Lexeme lex=null;  
	        while((lex=ik.next())!=null){  
	        	bw.write(lex.getLexemeText()+"|"); 
	        }  
		}

		bw.flush();
		bw.close();

		input.close();
	}

}
